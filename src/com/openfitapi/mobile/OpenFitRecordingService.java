/**
 * 
 */
package com.openfitapi.mobile;

import com.google.android.apps.mytracks.services.TrackRecordingService;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;

/**
 * @author Jon Monroe "Berkona" Solipsis Development ©Mar 12, 2012
 */
public class OpenFitRecordingService extends TrackRecordingService {

  /*
   * Adds OpenFit branding to notification
   */
  @Override
  protected void startForegroundService(Notification notification) {
    PendingIntent contentIntent = PendingIntent.getActivity(this, 0 /* requestCode */, new Intent(
        this, OpenFitMobi.class), 0 /* flags */);

    notification = new Notification(R.drawable.openfit_icon, null /* tickerText */,
        System.currentTimeMillis());

    notification.setLatestEventInfo(this, getString(R.string.my_tracks_app_name),
        getString(R.string.track_record_notification), contentIntent);

    super.startForegroundService(notification);
  }
}
