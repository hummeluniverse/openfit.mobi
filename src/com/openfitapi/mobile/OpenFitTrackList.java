/**
 * 
 */
package com.openfitapi.mobile;

import android.content.ContentUris;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.View.OnCreateContextMenuListener;
import android.widget.AdapterView;

import com.google.android.apps.mytracks.Constants;
import com.google.android.apps.mytracks.TrackList;
import com.google.android.apps.mytracks.content.TracksColumns;
import com.google.android.apps.mytracks.services.ServiceUtils;

/**
 * @author Jon Monroe "Berkona" Solipsis Development ©Mar 9, 2012
 */
public class OpenFitTrackList extends TrackList {

  private final OnCreateContextMenuListener contextMenuListener = new OnCreateContextMenuListener() {
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
      menu.setHeaderTitle(R.string.track_list_context_menu_title);
      AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
      contextPosition = info.position;
      trackId = listView.getAdapter().getItemId(contextPosition);
      menu.add(Menu.NONE, Constants.MENU_SHOW, Menu.NONE, R.string.track_list_show_on_map);
      menu.add(Menu.NONE, Constants.MENU_EDIT, Menu.NONE, R.string.track_list_edit_track);
      if (!ServiceUtils.isRecording(OpenFitTrackList.this, serviceConnection.getServiceIfBound(),
          preferences) || trackId != recordingTrackId) {
        String saveFileFormat = getString(R.string.track_list_save_file);
        String fileTypes[] = getResources().getStringArray(R.array.file_types);

        menu.add(Menu.NONE, OpenFitConstants.MENU_SEND_TO_OPENFIT, Menu.NONE,
            R.string.track_list_send_openfit);
        SubMenu save = menu.addSubMenu(Menu.NONE, Constants.MENU_WRITE_TO_SD_CARD, Menu.NONE,
            R.string.track_list_save_sd);
        save.add(Menu.NONE, Constants.MENU_SAVE_GPX_FILE, Menu.NONE,
            String.format(saveFileFormat, fileTypes[0]));
        save.add(Menu.NONE, Constants.MENU_SAVE_KML_FILE, Menu.NONE,
            String.format(saveFileFormat, fileTypes[1]));
        save.add(Menu.NONE, Constants.MENU_SAVE_CSV_FILE, Menu.NONE,
            String.format(saveFileFormat, fileTypes[2]));
        save.add(Menu.NONE, Constants.MENU_SAVE_TCX_FILE, Menu.NONE,
            String.format(saveFileFormat, fileTypes[3]));
        menu.add(Menu.NONE, Constants.MENU_DELETE, Menu.NONE, R.string.track_list_delete_track);
      }
    }
  };

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    getListView().setOnCreateContextMenuListener(contextMenuListener);
  }

  @Override
  public boolean onMenuItemSelected(int featureId, MenuItem item) {
    Intent intent;
    switch (item.getItemId()) {
      case OpenFitConstants.MENU_SEND_TO_OPENFIT:
        Uri uri = ContentUris.withAppendedId(TracksColumns.CONTENT_URI, trackId);
        Log.i("OpenFit", "SEND_URI: " + uri.toString());
        intent = new Intent(this, OpenFitSendActivity.class);
        intent.setDataAndType(uri, TracksColumns.CONTENT_ITEMTYPE);
        startActivity(intent);
        return true;
      default:
        return super.onMenuItemSelected(featureId, item);
    }
  }
}
