/**
 * 
 */
package com.openfitapi.mobile;

import com.google.android.apps.mytracks.Constants;

/**
 * @author Jon Monroe "Berkona" Solipsis Development ©Mar 9, 2012
 */
public class OpenFitConstants extends Constants {

  public static final int MENU_SEND_TO_OPENFIT = 118;

  public static final int AUTH_REQUEST = 1;

  public static final String OPENFIT_USERNAME = "openfitUser";
  public static final String OPENFIT_DOMAIN = "openfitDomain";
  public static final String OPENFIT_ARCANUM = "openfitArcanum";

  public static final int OPENFIT_DOMAIN_IDX = 0;
  public static final int OPENFIT_USER_IDX = 1;
  public static final int OPENFIT_ARCANUM_IDX = 2;
  public static final int OPENFIT_TRACK_IDX = 3;
}
