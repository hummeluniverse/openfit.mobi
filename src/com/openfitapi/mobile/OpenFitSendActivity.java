/**
 * 
 */
package com.openfitapi.mobile;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.openfitapi.mobile.OpenFitTask.AsyncTaskController;

import static com.google.android.apps.mytracks.Constants.*;
import static com.openfitapi.mobile.OpenFitConstants.*;

/**
 * @author Jon Monroe "Berkona" Solipsis Development ©Mar 9, 2012
 */
public class OpenFitSendActivity extends Activity implements AsyncTaskController<Boolean> {

  public static final int PROGRESS_DIALOG = 0;

  private ProgressDialog progressDialog;

  private SharedPreferences preferences;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    preferences = getSharedPreferences(SETTINGS_NAME, MODE_PRIVATE);
  }

  @Override
  protected void onStart() {
    super.onStart();
    uploadOrAuthenicate();
  }

  // Either upload, or ask for authenication.
  private void uploadOrAuthenicate() {
    String[] creds = getCredentials();

    // getCredentials returns null if the current account is not valid
    if (creds == null) {
      Intent authI = new Intent(this, OpenFitAuthenticateActivity.class);
      authI.setAction(getIntent().getAction());
      authI.setDataAndType(getIntent().getData(), getIntent().getType());
      Log.d("OpenFit", authI.toString());
      startActivityForResult(authI, AUTH_REQUEST);
    } else {
      ArrayList<String> mutableCreds = new ArrayList<String>();
      for (int i = 0; i < creds.length; i++) {
        mutableCreds.add(creds[i]);
      }
      mutableCreds.add(String.valueOf(ContentUris.parseId(getIntent().getData())));
      creds = mutableCreds.toArray(new String[mutableCreds.size()]);
      new OpenFitUploadTask(this).execute(creds);
    }
  }

  private String[] getCredentials() {

    String[] creds = { preferences.getString(OPENFIT_DOMAIN, ""),
        preferences.getString(OPENFIT_USERNAME, ""), preferences.getString(OPENFIT_ARCANUM, ""), };

    // One of the required fields is blank
    if (creds[0].length() == 0 || creds[1].length() == 0 || creds[2].length() == 0) { return null; }

    return creds;
  }

  @Override
  protected Dialog onCreateDialog(int id) {
    switch (id) {
      case PROGRESS_DIALOG:
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setIcon(android.R.drawable.ic_dialog_info);
        progressDialog.setTitle(getString(R.string.send_openfit_progress_title, "OpenFit"));
        progressDialog.setMax(100);
        progressDialog.setProgress(0);
        progressDialog.setCancelable(false);
        return progressDialog;
      default:
        return null;
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    Log.d("OpenFit", "onActivityResult and resultCode = " + resultCode);
    if (requestCode == AUTH_REQUEST) {
      if (resultCode == RESULT_OK) {
        uploadOrAuthenicate();
      } else {
        finish();
      }
    }
  }

  /*
   * (non-Javadoc)
   * @see com.openfitapi.mobile.OpenFitTask.asyncTaskController#onPreExecute()
   */
  @Override
  public void onPreExecute() {
    showDialog(PROGRESS_DIALOG);
  }

  /*
   * (non-Javadoc)
   * @see
   * com.openfitapi.mobile.OpenFitTask.asyncTaskController#onProgressUpdate(int)
   */
  @Override
  public void onProgressUpdate(int progressPercent) {
    progressDialog.setProgress(progressPercent);
  }

  /*
   * (non-Javadoc)
   * @see
   * com.openfitapi.mobile.OpenFitTask.asyncTaskController#onPostExecute(java
   * .lang.Object)
   */
  @Override
  public void onPostExecute(Boolean result) {
    Toast.makeText(this, (result ? R.string.openfit_send_success : R.string.openfit_send_failure),
        Toast.LENGTH_LONG).show();
    if (progressDialog.isShowing()) dismissDialog(PROGRESS_DIALOG);
    finish();
  }
}
