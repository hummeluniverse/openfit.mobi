/**
 * 
 */
package com.openfitapi.mobile;

import com.google.android.apps.mytracks.Constants;
import com.google.android.apps.mytracks.MyTracks;

import android.content.Intent;
import android.view.MenuItem;

/**
 * @author Jon Monroe "Berkona" Solipsis Development ©Mar 9, 2012
 */
public class OpenFitMobi extends MyTracks {

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    return handleSelectedOption(item)
        ? true
        : super.onOptionsItemSelected(item);
  }
  
  private boolean handleSelectedOption(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.menu_tracks: {
        this.startActivityForResult(new Intent(this, OpenFitTrackList.class),
                Constants.SHOW_TRACK);
        return true;
      }
    }
    return false;
  }
}
