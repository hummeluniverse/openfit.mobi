/**
 * 
 */
package com.openfitapi.mobile;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.google.android.apps.mytracks.content.MyTracksLocation;
import com.google.android.apps.mytracks.content.MyTracksProviderUtils;
import com.google.android.apps.mytracks.content.MyTracksProviderUtils.LocationIterator;
import com.google.android.apps.mytracks.content.Sensor.SensorDataSet;
import com.google.android.apps.mytracks.content.Track;
import com.google.android.apps.mytracks.stats.TripStatistics;

/**
 * @author Jon Monroe "Berkona" Solipsis Development ©Mar 10, 2012
 */
public class OpenFitUploadTask extends OpenFitTask<String, Boolean> {
  private static final int LOGGING_IN = 25;
  private static final int BUILDING_TRACK = 50;
  private static final int UPLOADING_TRACK = 75;
  private static final int LOGGING_OUT = 100;

  private OpenFitSession session;

  /**
   * @param owner
   */
  public OpenFitUploadTask(AsyncTaskController<Boolean> owner) {
    super(owner);
  }

  /*
   * (non-Javadoc)
   * @see com.openfitapi.mobile.OpenFitTask#performTask(P[])
   */
  /**
   * Params list: param[
   */
  @Override
  protected Boolean performTask(String... params) {
    session = null;
    try {
      publishProgress(LOGGING_IN);
      session = new OpenFitSession(params[OpenFitConstants.OPENFIT_DOMAIN_IDX]);
      session.login(params[OpenFitConstants.OPENFIT_USER_IDX],
          params[OpenFitConstants.OPENFIT_ARCANUM_IDX]);
      if (!session.isLoggedIn()) { return null; }

      // JSONObject response = session.makeRequest("fitnessActivities.json",
      // "GET", null);
      // JSONArray items = response.getJSONArray("items");
      // for (int i = 0; i < items.length(); i++) {
      // Log.i("OpenFit", "item[" + i + "]: " + items.get(i).toString());
      // JSONObject details =
      // session.getActivityDetails(items.getJSONObject(i).getString("uri"));
      // logObject("details", details);
      // }

      publishProgress(BUILDING_TRACK);
      ActivityBuilder activity = buildTrack(Long
          .valueOf(params[OpenFitConstants.OPENFIT_TRACK_IDX]));

      publishProgress(UPLOADING_TRACK);
      Log.i("OpenFit", session.createNewActivity(activity.build()).toString(2));

      return true;
    } catch (IOException io) {
      io.printStackTrace();
      return false;
    } catch (JSONException e) {
      e.printStackTrace();
      return false;
    } finally {
      publishProgress(LOGGING_OUT);
      if (session != null && session.isLoggedIn()) session.logout();
    }

  }

  private ActivityBuilder buildTrack(long trackId) throws JSONException {
    Context context = owner.getApplicationContext();
    MyTracksProviderUtils providerUtils = MyTracksProviderUtils.Factory.get(context);
    Track track = providerUtils.getTrack(trackId);
    ActivityBuilder builder = new ActivityBuilder();
    TripStatistics stats = track.getStatistics();
    LocationIterator locItr = providerUtils.getLocationIterator(trackId, -1, false,
        MyTracksProviderUtils.DEFAULT_LOCATION_FACTORY);

    builder.setStartTime(new Date(stats.getStartTime())).setTitle(track.getName())
        .setCategory(track.getCategory()).setNotes(track.getDescription())
        .setDuration(stats.getTotalTime() / 1000).setTotalDistance(stats.getTotalDistance())
        .setMaxSpeed(stats.getMaxSpeed()).setAvgSpeed(stats.getAverageSpeed())
        .setElevationGain(stats.getTotalElevationGain());
    
    long startTime = stats.getStartTime() / 1000;
    while (locItr.hasNext()) {
      Location l = locItr.next();
      long offset = (l.getTime() / 1000) - startTime;
      builder.addLocationPoint(offset, l.getLatitude(), l.getLongitude());
      if (l.hasAltitude()) {
        builder.addElevationPoint(offset, l.getAltitude());
      }
      if (l instanceof MyTracksLocation) {
        SensorDataSet dataset = ((MyTracksLocation) l).getSensorDataSet();
        if (dataset.hasHeartRate() && dataset.getHeartRate().hasValue()) {
          builder.addHeartRatePoint(offset, dataset.getHeartRate().getValue());
        }
        if (dataset.hasCadence() && dataset.getCadence().hasValue()) {
          builder.addCadencePoint(offset, dataset.getCadence().getValue());
        }
        if (dataset.hasPower() && dataset.getPower().hasValue()) {
          builder.addPowerPoint(offset, dataset.getPower().getValue());
        }
      }
    }
    locItr.close();

    // logObject("createdActivity", builder);
    return builder;
  }

  private void logObject(String tag, JSONObject object) {
    Iterator<String> itr = object.keys();
    while (itr.hasNext()) {
      String s = itr.next();
      try {
        if (object.get(s) instanceof JSONArray) {
          // logArray(tag + "[" + s + "]", object.getJSONArray(s));
        } else if (object.get(s) instanceof JSONObject) {
          logObject(tag + "[" + s + "]", object);
        } else {
          Log.i("OpenFit", tag + "[" + s + "]: " + object.get(s).toString());
        }
      } catch (JSONException e) {
        Log.e("OpenFit", "JSONException: " + e.toString());
      }
    }
  }

  private void logArray(String tag, JSONArray array) {
    for (int i = 0; i < array.length(); i++) {
      try {
        Log.i("OpenFit", tag + "[" + i + "]: " + array.get(i).toString());
      } catch (JSONException e) {
        Log.e("OpenFit", "JSONException: " + e.toString());
      }
    }
  }
}
